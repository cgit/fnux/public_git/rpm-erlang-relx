%global realname relx
%global upstream erlware

# Technically we're noarch, but our install path is not.
%global debug_package %{nil}

Name:     erlang-%{realname}
Version:  3.26.0
Release:  1%{?dist}
Summary:  Release assembler for Erlang/OTP Releases
License:  ASL 2.0
URL:      https://github.com/%{upstream}/%{realname}
Source0:  https://github.com/%{upstream}/%{realname}/archive/v%{version}/%{realname}-v%{version}.tar.gz
BuildRequires:  erlang-rebar
BuildRequires:  erlang-providers
BuildRequires:  erlang-erlware_commons
BuildRequires:  erlang-cf
BuildRequires:  erlang-bbmustache
BuildRequires:  erlang-getopt
Requires:       erlang-rebar
Requires:       erlang-providers
Requires:       erlang-erlware_commons
Requires:       erlang-cf
Requires:       erlang-getopt

%description
Relx assembles releases for an Erlang/OTP release. Given a release
specification and a list of directories in which to search for OTP applications
it will generate a release output.

%prep
%autosetup -n %{realname}-%{version}

# Fix harcoded dependency to match packaged version
sed -i 's/{bbmustache, "1.0.4"}/{bbmustache, "1.5.0"}/' rebar.config

# FIXME: for some reason eunit fails to compile test/rlx_test_utils.erl when
# using rebar2. We should be able to get ride of this line once rebar3 is
# properly packaged.
rm test/rlx_test_utils.erl

# FIXME: this module depends on rlx_test_utils, see above comment.
rm test/rlx_depsolver_tester.erl

%build
%{erlang_compile}

%install
%{erlang_install}

%check
%{erlang_test}

%files
%license LICENSE.md
%doc README.md examples
%{erlang_appdir}/

%changelog
* Sun Jul 15 2018 Timothée Floure <fnux@fedoraproject.org> - 3.26.0-1
- Let there be package
